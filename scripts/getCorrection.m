function [Cnew,spikenew,StimNew]=getCorrection(C_df,spike,Stims)

% This function corrects for the missing frames both in the spike matrix
% and the C_df matrix:
% IN: 
%   C_df: Calcium trace
%   spike: spikes
%   Stims: Stimulation frames.
% Out: 
%   Cnew: Corrected C_df 
%   spikenew: corrected spikes
%   StimNew: Corrected Stimulus

numCell = size(C_df,1);
numFrame = size(C_df,2);

% Initialize as empty arrays C_df and Spikenew
Cnew = [];
spikenew = [];

% Precalculate the average C_df using the previous and next frame at each
% stimulation time.
Cavg = (C_df(:,Stims)+C_df(:,Stims+1))/2;    
% For the spikes in the exact time of stimulation we put zeroes.
spikesAtStim = zeros(numCell,1);

for i = 0:length(Stims)   
    if(i==0)
        % Identify the beginning and the end frame where C_df and spikes
        % will be updated
        lb = 1;
        ub = Stims(i+1);
        Cnew=[Cnew C_df(:,lb:ub) Cavg(:,i+1)];
        spikenew = [spikenew spike(:,lb:ub) spikesAtStim];
    elseif(i==length(Stims))
        % Identify the beginning and the end frame where C_df and spikes
        % will be updated
        lb = Stims(i)+1;
        ub = numFrame;
        Cnew=[Cnew C_df(:,lb:ub)];
        spikenew = [spikenew spike(:,lb:ub)];
    else
        % Identify the beginning and the end frame where C_df and spikes
        % will be updated
        lb = Stims(i)+1;
        ub = Stims(i+1);
        Cnew=[Cnew C_df(:,lb:ub) Cavg(:,i+1)];
        spikenew = [spikenew spike(:,lb:ub) spikesAtStim];
    end
               
end

% Since we inserted a frame for each stimulus then the time frame at which
% each stimuls occurs is shifted by one for each stimulus.
StimNew = Stims + (1:length(Stims)); 