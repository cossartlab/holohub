close all; clear; clc;

% This script is the one used to generate several panels of Fig. 3. It
% calculates the order parameter in each epoch and coorelates it with te
% functional connectivity.

% addpath ../../../../../MATLAB/sigstar/
warning off

% This function select the cases that will be analyzed. Please check the 
% Function documentation to see what it does. Indices of definitive cases
% are given by variable casess, please note that te double "s" was 
% intended to avoid confusion with loaded variable named "cases"
casess = excludeCases;
 
% I define the p-value that will be used for stat. significance
pVal = 0.05;

% Select one case to plot
% Case 8: good to see the changes in the PSTH of r
% Case 31: good to see the change in the calcium PSTH
plotCell = 8;

for dd = 1:length(casess)
    
    % go through all the casess
    cellSelected = casess(dd)
    
    % load the pre-analyzed data for each cell,
    fileName = ['AnalysisCell_' num2str(cellSelected) '.mat'];
    load(fileName);
    
    %% Coefficient of Variation
    
    % Statistical test for Coefficient of variation    
    % Store the median CV for each epoch.
    ccv1(dd)=median(CV_Pre,'omitnan'); ccv2(dd)=median(CV_Dur,'omitnan'); ccv3(dd)=median(CV_Post,'omitnan');
    
    %% Firing Rate
    
    % Store the median value of Firing rate for each epoch
    fr1(dd)=median(Freq_Pre,'omitnan'); fr2(dd)=median(Freq_Dur,'omitnan'); fr3(dd) = median(Freq_Post,'omitnan');
    
    %% Kuramoto Stat
    
    % Statistical test for kuramoto order parameter for each epoch    
    % save the mean order parameter in each epoch.
    rr1(dd)=mean(rPre,'omitnan'); rr2(dd)=mean(rDur,'omitnan'); rr3(dd)=mean(rPost,'omitnan');
    
    %% Global Peaks
    
    % Statistical test checking the height of the Average Calcium Trace for
    % the population
    GPeak_Pre = globalHeight_Pre(globalHeight_Pre>0);
    GPeak_Dur = globalHeight_Dur(globalHeight_Dur>0);
    GPeak_Post = globalHeight_Post(globalHeight_Post>0);    
  
    % save the mean height of the average calcium trace in each epoch.
    GPeak1(dd)=mean(GPeak_Pre,'omitnan'); GPeak2(dd)=mean(GPeak_Dur,'omitnan'); GPeak3(dd)=mean(GPeak_Post,'omitnan');
    
    %% PSTH Stats Spikes
    
    % Average PSTH    
    PSTHavg_sp1(dd)=PSTHsp_Avg_Dur;
    PSTHavg_sp2(dd)=PSTHsp_Avg_Post;
    
    % Store the maximum of the averaged PSTH calculated using the spikes
    % from CASCADE, using only post-stimuli lag
    PSTHmax_sp1(dd)=max(PSTHsp_Dur_global(end/2:end));
    PSTHmax_sp2(dd)=max(PSTHsp_Post_global(end/2:end));
    
    %% PSTH Stats Events    
    
    PSTHavg_ev1(dd)=PSTHev_Avg_Dur;
    PSTHavg_ev2(dd)=PSTHev_Avg_Post;    
    

    % Store the maximum of the averaged PSTH calculated using the calcium
    % events (calcium maxima), using only post-stimuli lag    
    PSTHmax_ev1(dd)=max(PSTHev_Dur_global(end/2:end));
    PSTHmax_ev2(dd)=max(PSTHev_Post_global(end/2:end));
    
    %% PSTH Stats Kuramoto
    
    PSTHmax_r1(dd) = max(PSTHr_Dur(end/2:end));
    PSTHmax_r2(dd)= max(PSTHr_Post(end/2:end));
    
    % Store the average value of kuramoto order parameter of the population
    % using both pre-and post stimuli lags
    PSTHavg_r1(dd) = PSTHr_Dur_global;
    PSTHavg_r2(dd) = PSTHr_Post_global;
    
    % Store the maximum value of the kuramoto order parameter of the
    % population using only post-stimuli lags
    PSTHmaxDer_r1(dd) = max(PSTHdiffR_Dur(end/2:end));
    PSTHmaxDer_r2(dd) = max(PSTHdiffR_Post(end/2:end));
    
    
    %% Degree of the network
        
    % Calculates de position of the stimulated cell in the ranked
    % degree vector (% Percentile) using the binary connextivity matrix
    percentile_position_stim_cell_outB(dd) = find(idx_outDegB==doublecell)/numCells*100;
    percentile_position_stim_cell_inB(dd) = find(idx_inDegB==doublecell)/numCells*100;
    
    % Calculates de position of the stimulated cell in the ranked
    % degree vector (% Percentile) using the weighted connextivity matrix
    percentile_position_stim_cell_outW(dd) = find(idx_outDegW==doublecell)/numCells*100;
    percentile_position_stim_cell_inW(dd) = find(idx_inDegW==doublecell)/numCells*100;              
    
    %% Plot behavior
    
    if dd==plotCell
  
    % Concatenate the events in each epoch to have the whole raster of
    % events
    EventAll = [Events_Pre_glob Events_Dur_glob Events_Post_glob];
    EventsLocation = find(EventAll>0);        
        
        figure
        mymap = [1 1 1;0 0 0];
        colormap(mymap)
        
        subplot(4,1,1)
        hold on
        title('Inferred Spikes')
        imagesc(spikenums)
        for jj = 1:length(frames_whisker)
            plot([frames_whisker(jj) frames_whisker(jj)],[numCells numCells+10],'r')
        end
        for jj = 1:length(frames_stims)
            plot([frames_stims(jj) frames_stims(jj)],[numCells numCells+10],'b')
        end
        xlim([1 numFrames])
        
        subplot(4,1,2)
        hold on
        title('Calcium Events')
        imagesc([Events_Pre Events_Dur Events_Post])
        for jj = 1:length(frames_whisker)
            plot([frames_whisker(jj) frames_whisker(jj)],[numCells numCells+10],'r')
        end
        for jj = 1:length(frames_stims)
            plot([frames_stims(jj) frames_stims(jj)],[numCells numCells+10],'b')
        end
        %plot([0 numFrames],[Stim_cell Stim_cell],'--b')
        xlim([1 numFrames])
        
        subplot(4,1,3)
        hold on
        title('Global Activity')
        plot([global_Pre global_Dur global_Post])
        for jj = 1:length(frames_whisker)
            plot([frames_whisker(jj) frames_whisker(jj)],[1 1.5],'r')
        end
        for jj = 1:length(frames_stims)
            plot([frames_stims(jj) frames_stims(jj)],[1 1.5],'b')
        end
        xlim([1 numFrames])
        plot([EventsLocation; EventsLocation], repmat([0.8 1]',1,size(EventsLocation,2)), '-k')
        ylim([0 1.5])
        
        subplot(4,1,4)
        hold on
        title('Order Parameter')
        plot([rPre;rDur;rPost])
        for jj = 1:length(frames_whisker)
            plot([frames_whisker(jj) frames_whisker(jj)],[1 1.5],'r')
        end
        for jj = 1:length(frames_stims)
            plot([frames_stims(jj) frames_stims(jj)],[1 1.5],'b')
        end
        xlim([1 numFrames])
        ylim([0 1.5])
        
        %%
                
        figure        
        subplot(2,2,1)
        title('PSTH_c Stim')
        hold on
        surf(-window:window,1:numCells,PSTHev_Dur)
        view(2)
        shading flat
        xlim([-window window]); ylim([1 numCells]);
        set(gca,'CLim',[0 0.5])
        xlabel('Stimulus delay');
        ylabel('Cell #');        
        
        subplot(2,2,2)
        title('PSTH_c Inhib')
        hold on
        surf(-window:window,1:numCells,PSTHev_Post)
        view(2)
        shading flat
        xlim([-window window]); ylim([1 numCells]);
        set(gca,'CLim',[0 0.5])
        xlabel('Stimulus delay');
        ylabel('Cell #');        
        
        subplot(2,2,3:4)
        hold on
        plot(-window:window,PSTHev_Dur_global,'b')
        plot(-window:window,PSTHev_Post_global,'r')
        legend('Stim','Inhib.')
        xlim([-window window]);
        xlabel('Stimulus delay')
        ylabel('<PSTH_c>');        
        
        
        figure
        subplot(1,2,1)
        hold on
        title('$P_r$','Interpreter','Latex')
        plot(-window:window,PSTHr_Dur,'b')
        plot(-window:window,PSTHr_Post,'r')
        legend('$P^{st}_r$','$P^{in}_r$','Interpreter','Latex')
        xlim([-window window]);
        xlabel('Stimuls Delay','Interpreter','latex');
        ylabel('$P_r$','Interpreter','latex');
        
        subplot(1,2,2)
        hold on
        title('$\frac{d}{dt} P_r$','Interpreter','Latex')
        plot(-window+0.5:window-0.5,diff(PSTHr_Dur),'b')
        plot(-window+0.5:window-0.5,diff(PSTHr_Post),'r')
        xlim([-window window]);
        legend('$P^{st}_r$','$P^{in}_r$','Interpreter','Latex')
        xlabel('Stimuls Delay','Interpreter','latex');
        ylabel('$\frac{d}{dt} P_r$','Interpreter','Latex');    
        xlim([-window window])
        drawnow
        
        
        
    end

    
end


%% Pooled PSTH stats
pPooled = 0.05;

[~,pPSTHavgEv] = ttest(PSTHavg_ev1-PSTHavg_ev2);
if(pPSTHavgEv<pPooled)
    disp('pooled PSTH Avg Event Ok')
end

[~,pPSTHmaxEv] = ttest(PSTHmax_ev1-PSTHmax_ev2);
if(pPSTHmaxEv<pPooled)
    disp('pooled PSTH Max Event Ok')
end


[~,pPSTHavgSp] = ttest(PSTHavg_sp1-PSTHavg_sp2);
if(pPSTHavgSp<pPooled)
    disp('pooled PSTH Avg sp Ok')
end

[~,pPSTHmaxSp] = ttest(PSTHmax_sp1-PSTHmax_sp2);
if(pPSTHmaxSp<pPooled)
    disp('pooled PSTH Max sp Ok')
end


[~,pPSTHmaxR] = ttest(PSTHmax_r1-PSTHmax_r2);
if(pPSTHmaxR<pPooled)
    disp('pooled PSTH Max r Ok')
end


[~,pPSTHmaxDerR] = ttest(PSTHmaxDer_r1-PSTHmaxDer_r2);
if(pPSTHmaxDerR<pPooled)
    disp('pooled PSTH Diff r Ok')
end

[~,pPSTHAvgR] = ttest(PSTHavg_r1-PSTHavg_r2);
if(pPSTHAvgR<pPooled)
    disp('pooled PSTH Avg r Ok')
end


%% Correlation with Funcitonal connectivity

% Here we can change the B -> W to change the correlation from percentile
% position using the binarized Connectivity matrix or the weighted one.
CompareOUT = percentile_position_stim_cell_outW;
CompareIN = percentile_position_stim_cell_inW;

% Now we have only two epochs that are "comparable", not three, so baseline
% is considered rr2
baseline_r = rr2';
% This is the Delta(r).
diff_r = rr3'-mean(baseline_r,2);
% I run the correlate_functional function that essentially the figures to
% avoid a bunch of figures in the main script
%correlate_functional(diff_r,CompareOUT,CompareIN,'$\Delta \bar{r}$')

% The same is done for the firing rate
baseline_fr = fr2';
% This is the Delta(fr).
diff_fr = fr3'-mean(baseline_fr,2);
% I run the correlate_functional function that essentially the figures to
% avoid a bunch of figures in the main script
%correlate_functional(diff_fr,CompareOUT,CompareIN,'$\Delta \nu$')

% The quantities that were calculated using the PSTH are directly
% comparable, so no need to define baselines or anything. Here we compare
% the differences in the maxima of the order parameter PSTH
diff_PSTHmaxR = PSTHmax_r1-PSTHmax_r2;
% I run the correlate_functional function that essentially the figures to
% avoid a bunch of figures in the main script
correlate_functional(diff_PSTHmaxR',CompareOUT,CompareIN,'$\Delta r_{max}$')

% The quantities that were calculated using the PSTH are directly
% comparable, so no need to define baselines or anything. Here we compare
% the differences in the derivative of the order parameter PSTH
diff_PSTHderR = PSTHmaxDer_r1-PSTHmaxDer_r2;
% I run the correlate_functional function that essentially the figures to
% avoid a bunch of figures in the main script
correlate_functional(diff_PSTHderR',CompareOUT,CompareIN,'$\Delta dr_{max}$')

% The quantities that were calculated using the PSTH are directly
% comparable, so no need to define baselines or anything. Here we compare
% the differences in the maxima of the average Calcium PSTH.
diff_PSTHmaxEv = PSTHmax_ev1-PSTHmax_ev2;
% I run the correlate_functional function that essentially the figures to
% avoid a bunch of figures in the main script
correlate_functional(diff_PSTHmaxEv',CompareOUT,CompareIN,'$\Delta c_{max}$')

%% Control vs Photoinhibited cases:

coordLineStyle = 'k.';

% Maximum Average calcium PSTH
figure(100)
subplot(1,3,1)
hold on
title('max(PSTH_{calcium})')
boxplot([PSTHmax_ev1' PSTHmax_ev2'],'Symbol', coordLineStyle)
parallelcoords([PSTHmax_ev1' PSTHmax_ev2'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
    'Marker', '.', 'MarkerSize', 10);
xticks([1 2])
xticklabels({'Stim','Inhibition'})
sigstar({[1,2]},[pPSTHmaxEv])
drawnow

% Maximum Kuramoto order parameter
figure(100)
subplot(1,3,2)
hold on
title('max(PSTH_r)')
boxplot([PSTHmax_r1' PSTHmax_r2'],'Symbol', coordLineStyle)
parallelcoords([PSTHmax_r1' PSTHmax_r2'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
    'Marker', '.', 'MarkerSize', 10);
xticks([1 2])
xticklabels({'Stim','Inhibition'})
sigstar({[1,2]},[pPSTHmaxR])
drawnow

% Maximum Derivative of the PSTH of Kuramoto order parameter
figure(100)
subplot(1,3,3)
hold on
title('max(|d/dt PSTH_r|) ')
boxplot([PSTHmaxDer_r1' PSTHmaxDer_r2'],'Symbol', coordLineStyle)
parallelcoords([PSTHmaxDer_r1' PSTHmaxDer_r2'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
    'Marker', '.', 'MarkerSize', 10);
xticks([1 2])
xticklabels({'Stim','Inhibition'})
sigstar({[1,2]},[pPSTHmaxDerR])
drawnow
