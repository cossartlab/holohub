clear all; close all; clc

addpath ../scripts/
addpath ../../../../MATLAB/sigstar/
warning off

% with this script we can plot the pooled versions of the data analyzed in
% each type of cell and compare the results using 'Binary' FC or 'Weighted'
% with either 'Peak' or 'Onset' order parameter calculation. Notice that
% Onset Binary combination leads to a whole bunch of Network EffectCells in
% GABA, although at the expense of a smaller correlation significance and a
% less clear trend of the global effect in term of the paired boxplots
% This is up to us, though. What is more important to show?

typeFC = 'Binary';
typeEv = 'Onset';
corrType = 'Pearson';

%% Gluta

fileName = [typeEv 'Analysis_' typeFC];
load(['Figure3_Definitive/' fileName]);

figure
subplot(1,2,1)
hold on
[r,p]=corr(diff_r,CompareOUT','type',corrType);
title(['OUT r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareOUT,diff_r,'o')
lsline
plot(CompareOUT(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')

subplot(1,2,2)
hold on
[r,p]=corr(diff_r,CompareIN','type',corrType);
title(['IN r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareIN,diff_r,'o');
lsline
plot(CompareIN(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')
drawnow

pause(0.5)
figure
coordLineStyle = 'k.';
hold on
title('Pooled \langle r \rangle')
boxplot([rr1' rr2' rr3'],'Symbol', coordLineStyle)
parallelcoords([rr1' rr2' rr3'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
 'Marker', '.', 'MarkerSize', 10);

parallelcoords([rr1(NetworkEffectCells)' rr2(NetworkEffectCells)' rr3(NetworkEffectCells)'], 'Color', 'r', 'LineStyle', '-',...
 'Marker', '.', 'MarkerSize', 10);

xticks([1 2 3])
xticklabels({'Pre','Stim','Post'})
sigstar({[1,2],[2,3],[1,3]},[p1r,p2r,p3r])  

% Statistics of the average effect
averageBaseline = mean([rr1' rr3'],2);
PercentualDifference = (rr2' - averageBaseline)./averageBaseline * 100;

% For operational HUbs
disp('Gluta Cells')

disp(['Mean Percentual Difference of Hub Cells ' num2str(mean(PercentualDifference(NetworkEffectCells)))])
disp(['Std Percentual Difference of Hub Cells ' num2str(std(PercentualDifference(NetworkEffectCells)))])

% For all Cells
disp(['Mean Percentual Difference of all Cells ' num2str(mean(PercentualDifference))])
disp(['Std Percentual Difference of all Cells ' num2str(std(PercentualDifference))])

%% GABA

fileName = [typeEv 'Analysis_' typeFC];
load(['Figure4_Definitive/' fileName]);


figure
subplot(1,2,1)
hold on
[r,p]=corr(diff_r,CompareOUT','type',corrType);
title(['OUT r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareOUT,diff_r,'o')
lsline
plot(CompareOUT(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')

subplot(1,2,2)
hold on
[r,p]=corr(diff_r,CompareIN','type',corrType);
title(['IN r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareIN,diff_r,'o');
lsline
plot(CompareIN(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')
drawnow

pause(0.5)
coordLineStyle = 'k.';
figure
hold on
title('Pooled \langle r \rangle')
boxplot([rr1' rr2' rr3'],'Symbol', coordLineStyle)
parallelcoords([rr1' rr2' rr3'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
  'Marker', '.', 'MarkerSize', 10);

parallelcoords([rr1(NetworkEffectCells)' rr2(NetworkEffectCells)' rr3(NetworkEffectCells)'], 'Color', 'r', 'LineStyle', '-',...
 'Marker', '.', 'MarkerSize', 10);

xticks([1 2 3])
xticklabels({'Pre','Stim','Post'})
sigstar({[1,2],[2,3],[1,3]},[p1r,p2r,p3r])  

% Statistics of the average effect

averageBaseline = mean([rr1' rr3'],2);
PercentualDifference = (rr2' - averageBaseline)./averageBaseline * 100;

% For operational HUbs
disp('GABA Cells')

disp(['Mean Percentual Difference of Hub Cells ' num2str(mean(PercentualDifference(NetworkEffectCells)))])
disp(['Std Percentual Difference of Hub Cells ' num2str(std(PercentualDifference(NetworkEffectCells)))])

% For all Cells
disp(['Mean Percentual Difference of Hub Cells ' num2str(mean(PercentualDifference))])
disp(['Std Percentual Difference of Hub Cells ' num2str(std(PercentualDifference))])

%% SST

fileName = [typeEv 'Analysis_' typeFC];
load(['Figure5_Definitive/' fileName]);

figure
subplot(1,2,1)
hold on
[r,p]=corr(diff_r,CompareOUT','type',corrType);
title(['OUT r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareOUT,diff_r,'o')
lsline
plot(CompareOUT(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')

subplot(1,2,2)
hold on
[r,p]=corr(diff_r,CompareIN','type',corrType);
title(['IN r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareIN,diff_r,'o');
lsline
plot(CompareIN(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')
drawnow

pause(0.5)
coordLineStyle = 'k.';
figure
hold on
title('Pooled \langle r \rangle')
boxplot([rr1' rr2' rr3'],'Symbol', coordLineStyle)
parallelcoords([rr1' rr2' rr3'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
  'Marker', '.', 'MarkerSize', 10);
xticks([1 2 3])
xticklabels({'Pre','Stim','Post'})
sigstar({[1,2],[2,3],[1,3]},[p1r,p2r,p3r])  

% Statistics of the average effect

averageBaseline = mean([rr1' rr3'],2);
PercentualDifference = (rr2' - averageBaseline)./averageBaseline * 100;

% For operational HUbs
disp('SST Cells')

disp(['Mean Percentual Difference of Hub Cells ' num2str(mean(PercentualDifference(NetworkEffectCells)))])
disp(['Std Percentual Difference of Hub Cells ' num2str(std(PercentualDifference(NetworkEffectCells)))])

% For all Cells
disp(['Mean Percentual Difference of Hub Cells ' num2str(mean(PercentualDifference))])
disp(['Std Percentual Difference of Hub Cells ' num2str(std(PercentualDifference))])
