function def_list = excludeCases

% This function excludes cases accoridng to some defined criteria below
casos = 79:123;
% Cases 107 and 86 do not exist in the first place
casos(casos==107)=[];
casos(casos==86)=[];

% Empty vector for definitive list of cases
def = [];

for dd = 1:length(casos)
    
    cellSelected = casos(dd);
    
    % For the first 10 cases the thing is much simpler,
    fileName = ['AnalysisCell_' num2str(cellSelected) '.mat'];
    load(fileName);
    
    % We choose only those cases where the PSTH of the average calcium 
    % during the "control" epoch has a significant PEAK (mean + 2 std)        
    maxEpoch1 = max(PSTHev_Dur_global); 
    meanEpoch1 = mean(PSTHev_Dur_global); stdEpoch1 = std(PSTHev_Dur_global);
    
    %maxEpoch2 = max(PSTHev_Post_global); 
    %meanEpoch2 = mean(PSTHev_Post_global); stdEpoch2 = std(PSTHev_Post_global);
    if(maxEpoch1 > meanEpoch1+2*stdEpoch1)
        def = [def dd];
    end
    
end

def_list = casos(def);
