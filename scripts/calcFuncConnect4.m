function [pkBin,pkW,tmPk,linkXneurOUT,linkXneurIN]=calcFuncConnect4(matAttOn)
% This function calculates Functional connectivity based on the spike train
% that is input in the NxM matrix matAttOn. Here N is the number of cells,
% and M is the number of frames
%
%   OUT: 
%   pkBin: An NxN BINARY connectivity matrix
%   pkW: An NxN Weigthed connectivity matrix
%   tmPK: An NxN matrix of the Time lag where maximum correlation was found

% This is the time lag used
tmLg=4;
tmLgVec=-tmLg:tmLg;
numCell=size(matAttOn,1);

% Initialize the matrices
pkW=zeros(numCell,numCell);
pkBin = zeros(numCell,numCell);
tmPk=zeros(numCell,numCell);

% We shall test the spike counts with a uniform distribution across the
% lags. For this we construct the distribution for the KS test.
probComparisonKS = [-tmLg:tmLg; 1/(tmLg*2+1):1/(tmLg*2+1):1]';

for i=1:numCell-1
    x = matAttOn(i,:);
    if sum(x>0)
        for j=i+1:numCell
            y = matAttOn(j,:);
            c=xcorr(x,y,tmLg);
            
            % Here we generate a vector with lag coincidences that will be
            % tested with 2 different tests
            u = repelem(tmLgVec,round(c));
            
            if(~isempty(u) && length(u)>5)
                
                % First we test against normal distribution with zero mean
                [~,p,ci,~]=ttest(u);
                % We also test for uniform distribution using the already
                % generated distribution outside the loop
                [~,p2,~,~]=kstest(u,probComparisonKS);
                
                % If both test are passed with p<0.01 then
                if(p<0.01 && p2<0.01)
                    % Calculate again the crosscorrelation with biased
                    % normalization
                    c=xcorr(x,y,tmLg,'biased');
                    % Calculate where the average time lag is using the
                    % confidence interval given by the ttest above.
                    ax=mean(ci(:));
                    % We store the maximum correlation for the weighted
                    % matrices
                    mx = max(c);
                    % If the lag is negative, the i is connected to j
                    if(ax<0)
                        pkBin(i,j)=1;
                        pkW(i,j)=mx(1);
                        tmPk(i,j)=abs(ax);
                    % If the lag is positive then j is connected to i    
                    else
                        pkBin(j,i)=1;
                        pkW(j,i)=mx(1);
                        tmPk(j,i)=abs(ax);
                    end
                end
                
            end
            
        end
        
    end
end



W_ij=(pkW./min(sum(matAttOn,2)*ones(1,numCell),(sum(matAttOn,2)*ones(1,numCell))'));
W_ij(isnan(W_ij))=0;

xl=sum((sum(W_ij,2)>0)' | (sum(W_ij,1)>0));
linkXneurOUT{i}=sum((W_ij>0),2)*100/xl;
linkXneurIN{i}=sum((W_ij>0),1)'*100/xl;