function [C_norm]=getNormalization(C_df)

% This function smooths the C_df curves with a Gaussian kernel and
% normalizes traces to the interval [0 1]
% IN: C_df
% OUT: Smoothed and normalized C_df

numCells = size(C_df,1);

% Smoothing and normalization at each interval
% Prestimulus, we normalize
C_norm = zeros(size(C_df));
    for j = 1:numCells
        smoothTrace = smoothdata(C_df(j,:),'gaussian');
        C_norm(j,:)=(smoothTrace-min(smoothTrace))/(max(smoothTrace)-min(smoothTrace));        
    end
    
    
    
end
