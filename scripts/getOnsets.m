function [CalciumMaxLoc,CalciumHeight,OnsetLoc,globalCalciumLoc,globalCalciumHeight]=getOnsets(Df_signal,~)

% This function localices both C_df onset and C_df peaks using findpeaks.
% IN: C_df
% OUT: 
%   CalciumMaxLoc: Localization of Calcium Traces Maxima
%   CalciumHeight: Height of the Calcium Maxima
%   OnsetLoc: Localization of the Calcium Onset
%   globalCalciumLoc: Average Calcium activity maxima
%   globalCalciumHeight: Height of the average calcium maxima.

% Initialize arrays
numCells = size(Df_signal,1);
OnsetLoc = zeros(size(Df_signal));
CalciumMaxLoc = zeros(size(Df_signal));
CalciumHeight = CalciumMaxLoc;

% Precompute the average global activity
globalAct = mean(Df_signal);


for i = 1:numCells     
    % This is for the calculation of the calcium peaks
    signal = Df_signal(i,:);
    % Define a threshold to void small peaks
    th = mean(signal);
    % A minimum distance between peaks of 4 is used
    [height,loc]=findpeaks(signal,'MinPeakDistance',4,'MinPeakHeight',th); 
    CalciumMaxLoc(i,loc)=1;
    CalciumHeight(i,loc)=height;
        
    % This is for the calculation of the Calcium Onset
    % Compute the derivativr
    signal = diff(Df_signal(i,:));
    % Stick to the positive changes
    signal(signal<0) = 0;
    % Define a threshold to avoid small peaks
    th = mean(signal);
    % A minimum distance between peaks of 4 is used
    [~,loc]=findpeaks(signal,'MinPeakDistance',4,'MinPeakHeight',th); 
    OnsetLoc(i,loc)=1;    
    
end

% Peaks are also calculated for the global activity a prominence of 0.02.
% This is not very important because this result is not really used.
globalCalciumLoc = zeros(size(globalAct));
globalCalciumHeight=globalCalciumLoc;
[height,loc]=findpeaks(globalAct,'MinPeakDistance',4,'MinPeakHeight',0,'MinPeakProminence',0.02); 
globalCalciumLoc(loc)=1;
globalCalciumHeight(loc)=height;








