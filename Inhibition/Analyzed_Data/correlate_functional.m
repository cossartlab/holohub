
function correlate_functional(difference,CompareOUT,CompareIN,str)

figure

subplot(1,2,1)
hold on
[r,p]=corr(difference,CompareIN','type','Pearson');
title(['$\rho =$ ' num2str(round(r,2,'significant')) '; $p =$ ' num2str(round(p,2,'significant'))],'Interpreter','Latex')
plot(CompareIN,difference,'ok','MarkerFaceColor','k','MarkerSize',10);
h=lsline;
set(h,'Linewidth',2,'LineStyle','--','Color','r')
xlabel('$FC_{in}$ \%stim cell','Interpreter','Latex'); ylabel(str,'Interpreter','Latex');
set(gca,'FontSize',20)
box on

subplot(1,2,2)
hold on
[r,p]=corr(difference,CompareOUT','type','Pearson');
title(['$\rho =$ ' num2str(round(r,2,'significant')) '; $p =$ ' num2str(round(p,2,'significant'))],'Interpreter','Latex')
plot(CompareOUT,difference,'ok','MarkerFaceColor','k','MarkerSize',10)
h=lsline;
set(h,'Linewidth',2,'LineStyle','--','Color','r')
xlabel('$FC_{out}$ \%stim cell','Interpreter','Latex'); ylabel(str,'Interpreter','Latex');
set(gca,'FontSize',20)
box on


end
