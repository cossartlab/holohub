close all; clear; clc;

% This script is the one used to generate several panels of Fig. 4. It
% calculates the order parameter in each epoch and coorelates it with te
% functional connectivity.

% Be sure to add the path where the functions are
addpath ../scripts/
addpath ../../../../../MATLAB/sigstar/
warning off

% This is the identification of the GABA cases
cases = 1:26;
NetworkEffectCells = [];

for cc = 1:length(cases)
    
    cellSelected = cc
    % Load the cell
    fileName = ['Cell_' num2str(cellSelected) '.mat'];
    load(fileName);
 
    % This part here is used in case there are crappy cells that completely
    % mess up with the calculations. In particular cell 22 has serveral
    % bad cells. Be careful because this could mess up with label of the
    % stimulated cell. In this case however the label of the stimulated 
    % cell is smaller than the eliminated cells so it does not affect the
    % label.
    if cc == 22
        C_df([114 129 131],:)=[];
        spikenums([114 129 131],:)=[];
    end    
        
    numCells = size(spikenums,1);
    numFrames = size(spikenums,2);
    
    %% Data preprocessing:
    
    % Correct and normalize the data.
    [C_df,spikenums,Stims]=getCorrection(C_df,spikenums,Stims);                          
    C_norm = getNormalization(C_df);
    
    % Divide the C_df between Pre-stimuls, Stimuls and Post Stimulus
    Df_Pre=C_norm(:,1:Stims(1)-1);
    Df_Dur=C_norm(:,Stims(1):Stims(end));
    Df_Post=C_norm(:,Stims(end)+1:end);
    
    % Divide the global activity between Pre-stimuls, Stimuls and Post Stimulus
    global_Pre = mean(Df_Pre);
    global_Dur = mean(Df_Dur);
    global_Post = mean(Df_Post);
    
    % Divide the spikes between Pre-stimuls, Stimuls and Post Stimulus    
    Spikes_Pre = spikenums(:,1:Stims(1)-1);
    Spikes_Dur = spikenums(:,Stims(1):Stims(end));
    Spikes_Post = spikenums(:,Stims(end):end);        
    
    %% Calculate events from calcium trace
    
    % Calculate the "events" for each epoch.
    [Events_Pre,EventsHeight_Pre,OnsetLoc_Pre,Events_Pre_glob,globalHeight_Pre] = getOnsets(Df_Pre);     
    [Events_Dur,EventsHeight_Dur,OnsetLoc_Dur,Events_Dur_glob,globalHeight_Dur] = getOnsets(Df_Dur);   
    [Events_Post,EventsHeight_Post,OnsetLoc_Post,Events_Post_glob,globalHeight_Post] = getOnsets(Df_Post);         
        
    % Concatenate the events in each epoch to have the whole raster of
    % events
    EventAll = [Events_Pre_glob Events_Dur_glob Events_Post_glob];
    EventsLocation = find(EventAll>0);
    
    %% Calculate the FC from the pre-stimulation wndow
    
    % Select what we will use for FC calculation. In this case, prestim
    aux_spikes = Spikes_Pre;
    % Eliminate Nan's and/or Infs.
    aux_spikes(isnan(aux_spikes))=0;
    aux_spikes(aux_spikes==Inf)=0;
    % Calculate Functional Connectivity Matrix. It can be either Binary 
    % XCB or Weighted XCW.
    [XCB,XCW,~,~,~]=calcFuncConnect4(aux_spikes);  
    
    % Calculate the out and in degree if we want to change the analysis
    % with the wieghted matrix just change XCB with XCW below
    outDeg = sum(XCB,2)/numCells*100;
    inDeg = sum(XCB,1)/numCells*100;
    
    % Sort the data
    [~,idx_outDeg] = sort(outDeg);
    [~,idx_inDeg] = sort(inDeg);
    
    % Create a cell to save the out degree, in degree, number of cells and
    % stimulated cell of each case.
    outDegCell{cc}=outDeg;
    inDegCell{cc}=inDeg;
    CellTot{cc}=numCells;
    stimCellTot{cc}=Stim_cell;
        
    % Calculates de position of the stimulated cell in the ranked
    % degree vector (% Percentile)
    percentile_position_stim_cell_out(cc) = find(idx_outDeg==Stim_cell)/numCells*100;    
    percentile_position_stim_cell_in(cc) = find(idx_inDeg==Stim_cell)/numCells*100;    
    
    % Saves the out degree and in degree of the stimulated cell.
    outdegree_stimCell(cc) = outDeg(Stim_cell);    
    indegree_stimCell(cc) = inDeg(Stim_cell);           
    
    %% Synchronization analysis using events
    
    % Calculate the order parameter using the desired "events". In this
    % case I use the peaks of the calcium (Event_Pre).
    [rPre,phaseAvgPre,phaseAllPre]=calculateKuramoto(Events_Pre);
    [rDur,phaseAvgDur,phaseAllDur]=calculateKuramoto(Events_Dur);
    [rPost,phaseAvgPost,phaseAllPost]=calculateKuramoto(Events_Post);
        
    % We compare the median of the order parameter across epochs
    pr1 = ranksum(rPre,rDur);
    pr2 = ranksum(rDur,rPost);
    pr3 = ranksum(rPre,rPost);
    
    % If different at p<1e-4 level, cell is considered to have an effect 
    if(pr1<1e-4 && pr2<1e-4 && pr3>1e-4)
        disp(['Cell ' num2str(cc) ' has network effect'])
        NetworkEffectCells = [NetworkEffectCells cc];
    end
    
    % save the average order parameter in each epoch.
    rr1(cc)=mean(rPre,'omitnan'); rr2(cc)=mean(rDur,'omitnan'); rr3(cc)=mean(rPost,'omitnan');
    
    
    %% Plot selected cell
    
    if cellSelected==15
        
        figure
        mymap = [1 1 1;0 0 0];
        colormap(mymap)
        
        subplot(4,1,1)
        hold on
        title('Inferred Spikes')
        imagesc(spikenums)
        plot([Stims(1) Stims(1)],[1 numCells],'r')
        plot([Stims(end) Stims(end)],[1 numCells],'r')
        plot([0 numFrames],[Stim_cell Stim_cell],'--b')
        xlim([1 numFrames])
        
        subplot(4,1,2)
        hold on
        title('Calcium Events')
        imagesc([Events_Pre Events_Dur Events_Post])
        plot([Stims(1) Stims(1)],[1 numCells],'r')
        plot([Stims(end) Stims(end)],[1 numCells],'r')
        plot([0 numFrames],[Stim_cell Stim_cell],'--b')
        xlim([1 numFrames])
               
        subplot(4,1,3)
        hold on
        title('Global Activity')
        plot([global_Pre global_Dur global_Post])
        plot([Stims(1) Stims(1)],[0 1],'r')
        plot([Stims(end) Stims(end)],[0 1],'r')
        xlim([1 numFrames])
        plot([EventsLocation; EventsLocation], repmat([0.8 1]',1,size(EventsLocation,2)), '-k')   
        ylim([0 0.3])
        
        subplot(4,1,4)
        hold on
        title('Order Parameter')
        plot([rPre;rDur;rPost])
        plot([Stims(1) Stims(1)],[0 1],'r')
        plot([Stims(end) Stims(end)],[0 1],'r')
        xlim([1 numFrames])
        ylim([0 0.62])
        
        
        %% Violin plots of the order parameter for this cell
        g1 = repmat({'First'},length(rPre),1);
        g2 = repmat({'Second'},length(rDur),1);
        g3 = repmat({'Third'},length(rPost),1);
        g = [g1; g2; g3];
        
        figure
        hold on
        title('r distribution')
        violinplot([rPre;rDur;rPost],g)        
        sigstar({[1,2],[2,3],[1,3]},[pr1,pr2,pr3])
                
        
    end
    
end

%% Pooled Data 
% For the pooled data I perform a ttest between the difference between 
% average r across epochs.

[~,p1r] = ttest(rr1-rr2);
[~,p2r] = ttest(rr2-rr3);
[~,p3r] = ttest(rr1-rr3);
if(p1r<0.01 && p2r<0.01 && p3r>=0.01)
    disp('pooled synch Ok')
end


%% Global functional connectivity

% This piece of ode is to calculate the percentiles using the pooled data.
% Not really useful for this case
pooledDataOut = [];
pooledDataIn = [];
totCell = 0;

for cc = 1:length(cases)
    pooledDataOut = [pooledDataOut;outDegCell{cc}];
    pooledDataIn = [pooledDataIn;inDegCell{cc}'];
    stimCellNewIdx(cc)=stimCellTot{cc}+totCell;
    totCell = CellTot{cc}+totCell;
end

[rank_outDeg,idx_outDeg] = sort(pooledDataOut);
[rank_inDeg,idx_inDeg] = sort(pooledDataIn);

for cc = 1:length(cases)
    % Calculates de position of the stimulated cell in the ranked
    % correlation
    percentile_position_stim_cell_total_out(cc) = find(idx_outDeg==stimCellNewIdx(cc))/totCell*100;
    percentile_position_stim_cell_total_in(cc) = find(idx_inDeg==stimCellNewIdx(cc))/totCell*100;
end    
     

%% Comparison of percentiles

% This is the "baseline" order parameter using the data of epochs Pre and
% Post.
baseline_r = [rr1' rr3'];
% This is the Delta r.
diff_r = rr2'-median(baseline_r,2);

% This is what I want to correlate the Delta(r) with. For our analysis we
% used the percentile of the stimulated cell within its own recording
% session. If we want to compare with pooled percentile then change 
% percentile_position_stim_cell_out with percentile_position_stim_cell_total_out
% percentile_position_stim_cell_in with percentile_position_stim_cell_total_in
CompareOUT = percentile_position_stim_cell_out;
CompareIN = percentile_position_stim_cell_in;

figure
subplot(1,2,1)
hold on
[r,p]=corr(diff_r,CompareOUT','type','Pearson');
title(['OUT r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareOUT,diff_r,'o')
lsline
plot(CompareOUT(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
%plot([HC_out HC_out],[-1 1],'--r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')

subplot(1,2,2)
hold on
[r,p]=corr(diff_r,CompareIN','type','Pearson');
title(['IN r = ' num2str(r) ' p = ' num2str(p)])
plot(CompareIN,diff_r,'o');
lsline
plot(CompareIN(NetworkEffectCells),diff_r(NetworkEffectCells),'or','MarkerFaceColor','r')
%plot([HC_out HC_out],[-1 1],'--r')
xlabel('FC Percentile of stimulated cell'); ylabel('kuramoto parameter');
set(gca,'XScale','lin')

%% This is how average r changes across epochs

coordLineStyle = 'k.';
figure
hold on
title('Pooled \langle r \rangle')
boxplot([rr1' rr2' rr3'],'Symbol', coordLineStyle)
parallelcoords([rr1' rr2' rr3'], 'Color', 0.7*[1 1 1], 'LineStyle', '-',...
  'Marker', '.', 'MarkerSize', 10);
xticks([1 2 3])
xticklabels({'Pre','Stim','Post'})
sigstar({[1,2],[2,3],[1,3]},[p1r,p2r,p3r])   
