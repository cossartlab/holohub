function [r,phaseAvg,phase]=calculateKuramoto(events)

% This function calculates de order parameter r as a function of time. It
% also calculates the average phase of the neurons in time phaseAvg and the
% indiviudal phases of the oscillators in time phase.
% IN: Events, it takes the NxM matrix where N is the number of neurons and
% M is the number of frames. Each element is 1 or 0 depending if there is
% an "event" (could be calcium onset or calcium peak), very similar to the
% raster.


% Initialize arrays.
numCell = size(events,1);
phase = zeros(size(events));
numFrames = size(events,2);

% The Kuramoto Order Parameter has some degree of variability because I
% impose random behavior at the beginning and the end of the epochs. For
% reproducibility I impose seed random generator

rng('default');
rng(23);

for i = 1:numCell
    
    % Select events for each cell
    aux = events(i,:);
    % Check where it changes from burst to silence and viceversa, whenever
    % the difference of the raster is different than 0.
    aux2 = diff(aux);
    % indices where burst starts
    idxIni=find(aux2==1);
    % indices where burst ends
    idxEnd=find(aux2==-1);
    % Reconstruct the phases using a straight line equation y =
    % (y2-y1)/(x2-x1)*(x-x1)
    
    if(~isempty(idxIni))        
        x = 1:idxIni(1);
        % I assign a random phase to the initial and final frame so I avoid a large r
        % at the beginning and the end which may give rise to misleading
        % results        
        randPhase0 = 2*pi*rand;        
        phase(i,1:idxIni(1))=(2*pi-randPhase0)/(idxIni(1)-1)*(x-1) + randPhase0;
        
        for j = 1:length(idxIni)-1
            x = idxEnd(j):idxIni(j+1);
            phase(i,idxEnd(j):idxIni(j+1))=(2*pi-0)/(idxIni(j+1)-idxEnd(j))*(x-idxEnd(j));
        end

        % I also assign a random phase to the very last case to avoid again
        % very large r
        j = length(idxIni);
        x = idxEnd(j):numFrames;        
        randPhase0 = 2*pi*rand;        
        phase(i,idxEnd(j):numFrames)=(randPhase0-0)/(numFrames-idxEnd(j))*(x-idxEnd(j));

        
    end
    
end



% Orde parameter and average phase are calculated like so:
order_param=1/numCell*sum(exp(1i*phase),1);
r = abs(order_param);
phaseAvg = angle(order_param);

r = r';
phaseAvg = phaseAvg';